<!DOCTYPE html>
<html>
<head>
    <title><?php echo $data['item']->page_title; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Backstage::gi()->EXTERNAL_URL; ?>bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Backstage::gi()->EXTERNAL_URL; ?>bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" type="text/css" charset="utf-8" href="<?php echo Backstage::gi()->EXTERNAL_URL; ?>datetimepicker/bootstrap-datetimepicker.css" />
	<link rel="stylesheet" type="text/css" charset="utf-8" href="<?php echo Backstage::gi()->EXTERNAL_URL; ?>selectize/css/selectize.bootstrap3.css" />

    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>jquery/jquery.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>bootstrap/js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>jquery/jquery.print.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>typeahead/typeahead.bundle.min.js"></script>    
    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>typeahead/bloodhound.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>jquery/jquery.cookie.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>typeahead/handlebars-v2.0.0.js"></script>    
    
    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>jquery/json3.min.js"></script>
    
	<script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>selectize/js/standalone/selectize.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>jquery/jquery.serializeJSON.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>datetimepicker/moment.js"></script>
	<script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>datetimepicker/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>datetimepicker/locales/bootstrap-datetimepicker.en-gb.js"></script>
	<script type="text/javascript" charset="utf-8" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>datetimepicker/locales/bootstrap-datetimepicker.ru.js"></script>
    
    <meta name="viewport" content="width=device-width, user-scalable=false">
    <script type="text/javascript" charset="utf-8">
        var portal_url = '<?php echo Backstage::gi()->TEMPLATE_URL; ?>';
        var backstage_portal_url = '<?php echo Backstage::gi()->portal_url; ?>';
    </script>
</head>

<body>
  
    <div class="modal fade" id="ajax_error_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg">
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo Translations::gi()->close;?></span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo Translations::gi()->ajax_problem_header;?></h4>
        </div>
      <div class="modal-body" id="ajax_problem_body">
        <h4><span id="error_status"></span></h4>
        <button type="button" class="open" id="openDetails"><?php echo Translations::gi()->details;?></button>
        <button type="button" class="open" id="sendReport"><?php echo Translations::gi()->send_error_report;?></button>
        
      <div class="alert alert-success" id="alertSuccess" style="display:none;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span class="alertMessage"></span>
      </div>
      
      <div class="alert alert-danger" id="alertError" style="display:none;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span class="alertMessage"></span>
      </div>

        <textarea name="ajax_error_code" id="ajax_error_code" style="display:none;"></textarea>
      </div>
      <div class="modal-footer">
        <?php echo Translations::gi()->ajax_problem_body;?>
        <br>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Translations::gi()->close;?></button>
      </div>
      </div>
    </div>
  </div>

