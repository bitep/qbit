ALTER TABLE `form_fields` ADD COLUMN `has_placeholder` smallint(1) NOT NULL DEFAULT 0 AFTER `datetime`;
ALTER TABLE `form_fields` ADD COLUMN `field_class` varchar(255) DEFAULT NULL AFTER `field_name`;
ALTER TABLE `form_fields` ADD COLUMN `tags_table` varchar(255) DEFAULT NULL AFTER `field_width`;
ALTER TABLE `users` ADD COLUMN `status` tinyint(1) NOT NULL DEFAULT 1 AFTER `about`;
ALTER TABLE `layouts` DROP COLUMN `design_id`;
ALTER TABLE `form_field_values` ADD CONSTRAINT `fk_fi_field_id` FOREIGN KEY (`field_id`) REFERENCES `form_fields` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `form_field_values` ADD INDEX `idx_form_field_row` (`form_id`, `field_id`, `row_id`) USING BTREE;