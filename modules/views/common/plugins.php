<script src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>ckeditor/ckeditor.js"></script>
<script src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>ckfinder/ckfinder.js"></script>
<script src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>fileuploader/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>fileuploader/js/jquery.iframe-transport.js"></script>
<script src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>fileuploader/js/jquery.fileupload.js"></script>
<script src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>fileuploader/js/jquery.fileupload-process.js"></script>
<script src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>fileuploader/js/jquery.fileupload-validate.js"></script>
<script src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>jquery/jquery.sortable.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo Backstage::gi()->EXTERNAL_URL; ?>fileuploader/css/jquery.fileupload.css" />
<!--
<script src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>uploadify/jquery.uploadify.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo Backstage::gi()->EXTERNAL_URL; ?>uploadify/uploadify.css" />
-->

<script>
$(function () 
{
	CKFinder.setupCKEditor(null, '<?php echo Backstage::gi()->EXTERNAL_URL; ?>ckfinder/');	
});
</script>
<!--
<script type="text/javascript" src="<?php echo Backstage::gi()->EXTERNAL_URL; ?>tinymce/tinymce.min.js"></script>
-->
