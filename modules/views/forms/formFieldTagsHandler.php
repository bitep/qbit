<script>
    function deleteTag(tag, obj)
    {
        if (confirm("Are you sure?"))
        {
            //$(obj).attr('tag_name')};
            $(obj).parent().remove();
            var tags = $('#tagskeeper_<?php echo $request->parameters['field_name']?>').val()==''?[]:JSON.parse($('#tagskeeper_<?php echo $request->parameters['field_name']?>').val());
            tags.splice($.inArray(tag, tags),1);
            $('#tagskeeper_<?php echo $request->parameters['field_name']?>').val(JSON.stringify(tags));
        }
    }

    $(function()
    {	// UNIVERSAL TYPEAHEAD
        var <?php echo $request->parameters['field_name']; ?>_tags = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo Backstage::gi()->portal_url ?>forms/getTags',
                replace: function(url, query) {
                    return url + "?q=" + encodeURIComponent(query) + "&tags_table=<?php echo $request->parameters['tags_table'];?>";
                }
            },
            limit:100
        });

        <?php echo $request->parameters['field_name']; ?>_tags.initialize();

        $('input:text[name=<?php echo $request->parameters['field_name']; ?>]').typeahead(null,
            {
                name: '<?php echo $request->parameters['field_name']; ?>_tags',
                displayKey: 'tag_name',
                source: <?php echo $request->parameters['field_name']; ?>_tags.ttAdapter(),
                templates: {
                    empty: [
                        '<div class="empty-message">',
                        '<?php echo Translations::gi()->cant_find ?>',
                        '</div>'
                    ].join('\n'),
                    suggestion: Handlebars.compile('<p><strong>{{tag_name}}</strong></p>')
                }
            }).on('typeahead:selected', function (obj, datum)
        {
            var tags = $('#tagskeeper_<?php echo $request->parameters['field_name']?>').val()==''?[]:JSON.parse($('#tagskeeper_<?php echo $request->parameters['field_name']?>').val());
            if ($.inArray(datum.tag_name, tags) == -1)
            {
                $('#<?php echo $request->parameters['field_name']?>_tag_names').append('<h4 data-id="'+datum.tag_name+'"><span class="label label-warning" style="float: left; margin-left: 2px;">'+datum.tag_name+' <a href="#dummy" onclick="deleteTag(\''+datum.tag_name+'\', this);"><span style="vertical-align: top;" class="glyphicon glyphicon-remove"></span></a></span></h4>');
                tags.push(datum.tag_name);
                $('#tagskeeper_<?php echo $request->parameters['field_name']?>').val(JSON.stringify(tags));
            }
            $(this).typeahead('val','');
        }).on('keydown', function(e) {
            if (e.keyCode == 13 && $(this).val() != '') {
                var tag_val = $(this).val();
                var tags = $('#tagskeeper_<?php echo $request->parameters['field_name']?>').val()==''?[]:JSON.parse($('#tagskeeper_<?php echo $request->parameters['field_name']?>').val());
                if ($.inArray(tag_val, tags) == -1)
                {
                    $('#<?php echo $request->parameters['field_name']?>_tag_names').append('<h4 data-id="'+tag_val+'"><span class="label label-warning" style="float: left; margin-left: 2px;">'+tag_val+' <a href="#dummy" onclick="deleteTag(\''+tag_val+'\', this);"><span style="vertical-align: top;" class="glyphicon glyphicon-remove"></span></a></span></h4>');
                    tags.push(tag_val);
                    $('#tagskeeper_<?php echo $request->parameters['field_name']?>').val(JSON.stringify(tags));
                }
                $(this).typeahead('val','');
            }
        });
        //END UNIVERSAL TYPEAHEAD
    });
</script>
<?php
// Get tag ids if any
$html_struct = '';
$tags_val = '';
if ($request->parameters['source_id'] > 0)
{
    $js_tags = array();
    $tags = Loader::gi()->callModule('GET', 'forms/getFormFieldTags', array('tags_table'=>$request->parameters['tags_table'], 'field_id'=>$request->parameters['field_id'], 'source_id'=>$request->parameters['source_id']));
    foreach ($tags['body'] as $tag)
    {
        $html_struct .= '<h4 data-id="'.$tag->tag_name.'"><span class="label label-warning" style="float: left; margin-left: 2px;">'.$tag->tag_name.' <a href="#dummy" onclick="deleteTag(\''.$tag->tag_name.'\', this);"><span style="vertical-align: top;" class="glyphicon glyphicon-remove"></span></a></span></h4>';
        $js_tags[] = $tag->tag_name;
    }
    $tags_val = htmlspecialchars(json_encode($js_tags));
}
?>
<div id="<?php echo $request->parameters['field_name']?>_tag_names"><?php echo $html_struct;?></div>
<input type="hidden" id="tagskeeper_<?php echo $request->parameters['field_name']?>" name="tagskeeper_<?php echo $request->parameters['field_name']?>" value="" />
<input type="hidden" id="tagstable_<?php echo $request->parameters['field_name']?>" name="tagstable_<?php echo $request->parameters['field_name']?>" value="<?php echo $request->parameters['tags_table'];?>" />