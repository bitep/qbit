<?php
$locale_arr = array (
"language" => "English",
"template" => array (
        // Maintenance Buttons
		"T_GO_PRINT" => "Çap et",
		"T_GO_BACK" => "Qayıt",
		"T_GO_REFRESH" => "Yenilə",
		"T_NO_DATA_FOUND" => "Kriteriyaya görə heç bir məlumat tapılmamışdır",
		"T_UNABLE_TO_CONTINUE" => "Davam mümkünsüzdür",
		"T_INFORMATION" => "İnformasiya",
        )
);
?>
