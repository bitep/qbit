UPDATE `translations` SET ru='Страница не существует.' WHERE field_name='notfound';
UPDATE `translations` SET ru='Все права защищены.' WHERE field_name='copyright';
UPDATE `translations` SET ru='Добро пожаловать в систему' WHERE field_name='welcome';
UPDATE `translations` SET ru='Создано' WHERE field_name='poweredby';
UPDATE `translations` SET ru='Войти' WHERE field_name='log_in';
UPDATE `translations` SET ru='Ваш логин' WHERE field_name='your_login';
UPDATE `translations` SET ru='Ваш пароль' WHERE field_name='your_password';
UPDATE `translations` SET ru='Вы вошли в систему как' WHERE field_name='you_entered';
UPDATE `translations` SET ru='Логин' WHERE field_name='login';
UPDATE `translations` SET ru='Выйти' WHERE field_name='logout';
UPDATE `translations` SET ru='Пароль' WHERE field_name='password';
UPDATE `translations` SET ru='Регистрация' WHERE field_name='registration';
UPDATE `translations` SET ru='Вы успешно вошли в систему! Пересылка...' WHERE field_name='login_success';
UPDATE `translations` SET ru='Логин и/или пароль неверные! Пожалуйста, повторите попытку.' WHERE field_name='login_error';
UPDATE `translations` SET ru='Выход из системы...' WHERE field_name='logout_success';
UPDATE `translations` SET ru='Произошла ошибка! Пожалуйста, повторите попытку.' WHERE field_name='logout_error';
UPDATE `translations` SET ru='Макеты' WHERE field_name='layouts';
UPDATE `translations` SET ru='Страницы' WHERE field_name='pages';
UPDATE `translations` SET ru='Контенты' WHERE field_name='contents';
UPDATE `translations` SET ru='Блоги' WHERE field_name='blogs';
UPDATE `translations` SET ru='Галереи' WHERE field_name='galleries';
UPDATE `translations` SET ru='Каталоги' WHERE field_name='catalogs';
UPDATE `translations` SET ru='Дизайны' WHERE field_name='designs';
UPDATE `translations` SET ru='Формы' WHERE field_name='forms';
UPDATE `translations` SET ru='Профайлы' WHERE field_name='profiles';
UPDATE `translations` SET ru='Видео' WHERE field_name='streaming';
UPDATE `translations` SET ru='ВЕРСИЯ' WHERE field_name='current_version';
UPDATE `translations` SET ru='Проверка наличия обновлений на сервере...' WHERE field_name='check_server_for_upd';
UPDATE `translations` SET ru='Найдено обновление: v' WHERE field_name='new_update_v';
UPDATE `translations` SET ru='Загрузка обновлений...' WHERE field_name='downloading_new_upd';
UPDATE `translations` SET ru='Невозможно сохранить обновления. Операция отменена.' WHERE field_name='couldnt_save_upd_abort';
UPDATE `translations` SET ru='Обновления загружены и сохранены' WHERE field_name='upd_downloaded_and_saved';
UPDATE `translations` SET ru='Обновления уже загружены.' WHERE field_name='upd_already_downloaded';
UPDATE `translations` SET ru='Создание директории' WHERE field_name='created_directory';
UPDATE `translations` SET ru='ВЫПОЛНЕНО' WHERE field_name='executed';
UPDATE `translations` SET ru='ОБНОВЛЕНО' WHERE field_name='updated';
UPDATE `translations` SET ru='Установить' WHERE field_name='install_now';
UPDATE `translations` SET ru='Доступных обновлений нет.' WHERE field_name='no_upd_available';
UPDATE `translations` SET ru='Не удалось найти последние релизы.' WHERE field_name='couldnt_find_latest_rel';
UPDATE `translations` SET ru='Обновлено до v' WHERE field_name='updated_to_v';
UPDATE `translations` SET ru='Обновления готовы' WHERE field_name='update_ready';
UPDATE `translations` SET ru='Установлено' WHERE field_name='installed';
UPDATE `translations` SET ru='Не установлено' WHERE field_name='not_installed';
UPDATE `translations` SET ru='Эти обновления доступны только с лицензионным ключём' WHERE field_name='upd_avail_with_lic';
UPDATE `translations` SET ru='Проверить' WHERE field_name='check';
UPDATE `translations` SET ru='Лицензия успешно проверена. Установка обновлений.' WHERE field_name='lic_passes_checking';
UPDATE `translations` SET ru='ОШИБКА при проверке лицензии.' WHERE field_name='lic_error';
UPDATE `translations` SET ru='Этот Лицензионный ключ уже был использован.' WHERE field_name='lic_already_used';
UPDATE `translations` SET ru='Проверка Лицензионного ключа. Пожалуйста подождите...' WHERE field_name='checking_lic_key';
UPDATE `translations` SET ru='Лицензионный ключ был создан не для этого файла' WHERE field_name='not_this_file_lic';
UPDATE `translations` SET ru='Добавить запись' WHERE field_name='add_note';
UPDATE `translations` SET ru='Удалить' WHERE field_name='delete';
UPDATE `translations` SET ru='Закрыть' WHERE field_name='close';
UPDATE `translations` SET ru='Сохранить' WHERE field_name='save';
UPDATE `translations` SET ru='Вы уверены?' WHERE field_name='are_you_sure';
UPDATE `translations` SET ru='Похожий' WHERE field_name='similar';
UPDATE `translations` SET ru='Другое' WHERE field_name='different';
UPDATE `translations` SET ru='Больше' WHERE field_name='greater';
UPDATE `translations` SET ru='Меньше' WHERE field_name='less';
UPDATE `translations` SET ru='Разрешения' WHERE field_name='grants';
UPDATE `translations` SET ru='Настройки' WHERE field_name='settings';
UPDATE `translations` SET ru='Конфигурация' WHERE field_name='configurations';
UPDATE `translations` SET ru='Обновление' WHERE field_name='update';
UPDATE `translations` SET ru='Пользователи' WHERE field_name='users';
UPDATE `translations` SET ru='Переводы' WHERE field_name='translations';
UPDATE `translations` SET ru='Подписчики' WHERE field_name='subscribers';
UPDATE `translations` SET ru='Типы разрешений, устанавливаемых на ресурсы' WHERE field_name='grant_resource_types';
UPDATE `translations` SET ru='Для пользователей' WHERE field_name='for_users';
UPDATE `translations` SET ru='Для ролей' WHERE field_name='for_roles';
UPDATE `translations` SET ru='Выборки форм' WHERE field_name='form_selects';
UPDATE `translations` SET ru='Форма рассылки' WHERE field_name='mailing_form';
UPDATE `translations` SET ru='Письмо успешно отправлено' WHERE field_name='mail_sent_successfull';
UPDATE `translations` SET ru='При отправке письма возникли неполадки' WHERE field_name='mail_send_failed';
UPDATE `translations` SET ru='Просмотр' WHERE field_name='view';
UPDATE `translations` SET ru='Редактировать' WHERE field_name='edit';
UPDATE `translations` SET ru='Распечатать' WHERE field_name='print';
UPDATE `translations` SET ru='Поиск' WHERE field_name='search';
UPDATE `translations` SET ru='Сбросить' WHERE field_name='reset';
UPDATE `translations` SET ru='Общее количество' WHERE field_name='total_count';
UPDATE `translations` SET ru='Выбрать файлы' WHERE field_name='select_files';
UPDATE `translations` SET ru='Все' WHERE field_name='all';
UPDATE `translations` SET ru='Пакеты разрешений' WHERE field_name='grants_packs';
UPDATE `translations` SET ru='Читать дальше' WHERE field_name='read_more';