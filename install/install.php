<?php
/**
 * @package    installer
 *
 * @copyright  Copyright 2014 Rinat Gazikhanov, Vusal Khalilov, BITEP LLC. All rights reserved.
 * @license    GNU General Public License version 3 or later; see LICENSE.txt
 */

header('Content-Type: application/json');

//error_reporting(0);
$materials_status = '';

if (!empty($_POST['portal_url']) && !empty($_POST['db_host']) && !empty($_POST['db_name']) && !empty($_POST['db_user']) && !empty($_POST['db_pass'])) 
{
		// Get the empty config file
    $emptyConfig = file_get_contents("config.php-dist");

    $emptyConfig = str_replace('"db_host" => ""', '"db_host" => "' . $_POST['db_host'] . '"', $emptyConfig);
    $emptyConfig = str_replace('"db_name" => ""', '"db_name" => "' . $_POST['db_name'] . '"', $emptyConfig);
    $emptyConfig = str_replace('"db_user" => ""', '"db_user" => "' . $_POST['db_user'] . '"', $emptyConfig);
    $emptyConfig = str_replace('"db_pass" => ""', '"db_pass" => "' . $_POST['db_pass'] . '"', $emptyConfig);

    if (isset($_POST['db_table_prefix']) && !empty($_POST['db_table_prefix'])) 
	{
        if (substr($_POST['db_table_prefix'], -1) == "_") 
		{
            $_POST['db_table_prefix'] = rtrim($_POST['db_table_prefix'], "_");
        }
        $_POST['db_table_prefix'] = $_POST['db_table_prefix'] . "_";
    } 
	else 
	{
        $emptyConfig = str_replace('"db_table_prefix" => ""', '"db_table_prefix" => ""', $emptyConfig);
    }
	
    $emptyConfig = str_replace('"db_table_prefix" => ""', '"db_table_prefix" => "' . $_POST['db_table_prefix'] . '"', $emptyConfig);

    $emptyConfig = str_replace('"portal_email" => ""', '"portal_email" => "' . $_POST['portal_email'] . '"', $emptyConfig);
    $emptyConfig = str_replace('"portal_name" => ""', '"portal_name" => "' . $_POST['portal_name'] . '"', $emptyConfig);
    $emptyConfig = str_replace('"portal_urls" => ""', '"portal_urls" => "' . $_POST['portal_url'] . '"', $emptyConfig);
    $emptyConfig = str_replace('"template_name" => ""', '"template_name" => "' . $_POST['template_name'] . '"', $emptyConfig);
	
    $default_lang_parts = explode("|", $_POST['portal_default_lang']);
	
    $emptyConfig = str_replace('"portal_default_lang" => ""', '"portal_default_lang" => "' . $default_lang_parts[0] . '"', $emptyConfig);

    if (!empty($_POST['portal_langs'])) 
	{
        $langsShortArray = array();
        foreach ($_POST['portal_langs'] as $lang) {
            $lang = explode('|', $lang);
            $langsShortArray[] = $lang[0];
        }

        $emptyConfig = str_replace('"portal_langs" => ""', '"portal_langs" => "' . implode(",", $langsShortArray) . '"', $emptyConfig);
    } 
	else 
	{
        $default_lang_parts = explode("|", $_POST['portal_default_lang']);
        $emptyConfig = str_replace('"portal_langs" => ""', '"portal_langs" => "' . $default_lang_parts[0] . '"', $emptyConfig);
    }
		
		// Make a connection to DB
    $connect_str = 'mysql:host='.$_POST['db_host'].';port='.$_POST['db_port'].';dbname=' . $_POST['db_name'] . ';charset=utf8';
	$status = '';
	
    try 
	{
        $db = new PDO($connect_str, $_POST['db_user'], $_POST['db_pass'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } 
	catch (PDOException $e) 
	{
        if ($e->getCode() == '2002')
            $status = "DB Error: Unknown host";
		elseif ($e->getCode() == '1049')
		{
				// Creating a new DB
			try 
			{
				$db = new PDO("mysql:host=".$_POST['db_host'].';port='.$_POST['db_port'], $_POST['db_user'], $_POST['db_pass'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$db->exec("CREATE DATABASE `{$_POST['db_name']}`");
				$db->exec("USE `{$_POST['db_name']}`");
			}
			catch (PDOException $e) 
			{
				if ($e->getCode() == '2002') 
					$status = "DB Error: Unknown host";
				elseif ($e->getCode() == '1045') 
					$status = "DB Error: User error";
				else
					$status = "DB Error: Unknown DB error status ".$e->getMessage();
			}
		}
		else
			$status = "DB Error: Unknown DB error status ".$e->getMessage();
		if ($status != '')
		{
			echo '{"status":"' . $status . '"}';
			die();
		}
    }
	
	try
	{
			// Open base SQL queries and format them
		$sql = file_get_contents('qbit3-clean.sql');

		$sql = str_replace("DROP TABLE IF EXISTS `", "DROP TABLE IF EXISTS `" . @$_POST['db_table_prefix'] . "", $sql);
		$sql = str_replace("CREATE TABLE `", "CREATE TABLE `" . @$_POST['db_table_prefix'] . "", $sql);
		$sql = str_replace("INSERT INTO `", "INSERT INTO `" . @$_POST['db_table_prefix'] . "", $sql);
				
		$qr = $db->exec($sql);

			// Check if the language columns exist in translations
		foreach ($_POST['portal_langs'] as $colNames) 
		{
			$colNames = substr($colNames, 0, strpos($colNames, "|"));
			$findColumn = "DESCRIBE ".@$_POST['db_table_prefix']."translations;";

			$table_fields = $db->query($findColumn);
			$result = $table_fields->fetchAll(PDO::FETCH_OBJ);

			$colExists = false;
			foreach ($result as $item)
			{
				if ($item->Field == $colNames)
					$colExists = true;
			}
			if (!$colExists)
			{
				$addColumn = "ALTER TABLE " . @$_POST['db_table_prefix'] . "translations ADD " . $colNames . " LONGTEXT;";
				$qr = $db->exec($addColumn);
			}
		}

		if (!empty($_POST['portal_langs'])) 
		{
			foreach ($_POST['portal_langs'] as $langLong) 
			{
				$langLong = explode('|', $langLong);
				$sql = "INSERT INTO `" . @$_POST['db_table_prefix'] . "languages` VALUES (0, '" . $langLong[0] . "', '" . $langLong[1] . "');";
				$qr = $db->exec($sql);
			}
		} 
		else 
		{
			$langLong = explode('|', $_POST['portal_default_lang']);
			$sql = "INSERT INTO `" . @$_POST['db_table_prefix'] . "languages` VALUES (0, '" . $langLong[0] . "', '" . $langLong[1] . "');";
			$qr = $db->exec($sql);
		}

		$sql = "INSERT INTO `" . @$_POST['db_table_prefix'] . "users` VALUES ('1', '" . $_POST['admin_login'] . "', '" . md5($_POST['admin_password']) . "', '', '', '', '" . $_POST['admin_email'] . "', '', '1', '0');";
		$qr = $db->exec($sql);
	} 
	catch (PDOException $e) 
	{
		$status = "DB Error: Unknown DB error status ".$e->getMessage();
		echo '{"status":"' . $status . '"}';		
		die();
	}
		// Insall basic translations
	try
	{
		$static_translations_add = file_get_contents("translations/translations_structure.sql");
		$static_translations_add = str_replace("INSERT INTO `", "INSERT INTO `" . @$_POST['db_table_prefix'] . "", $static_translations_add);
		$qr = $db->exec($static_translations_add);

		foreach ($_POST['portal_langs'] as $langLong) 
		{
			$langLong = substr($langLong, 0, strpos($langLong, "|"));
			$static_translations = file_get_contents("translations/" . $langLong . ".sql");
			$static_translations = str_replace("ALTER TABLE `", "ALTER TABLE `" . @$_POST['db_table_prefix'] . "", $static_translations);
			$static_translations = str_replace("UPDATE `", "UPDATE `" . @$_POST['db_table_prefix'] . "", $static_translations);
			$qr = $db->exec($static_translations);
		}
	}
	catch (PDOException $e) 
	{
		echo '{"status":"DB Error:' . $e->getMessage() . '"}';
		die();
	}
	
		// Install DB of the choosen template 
	try
	{
		if (!empty($_POST['template_name']) && $_POST['template_name'] != 'default') 
		{		
			$data_structure = file_get_contents("data/" . $_POST['template_name'] . "/data_structure.sql");
			$data_structure = str_replace("INSERT INTO `", "INSERT INTO `" . @$_POST['db_table_prefix'], $data_structure);
			$db->exec($data_structure);

			foreach ($_POST['portal_langs'] as $langData) 
			{
				$langsDatas = explode('|', $langData);
				$demoData = file_get_contents("data/" . $_POST['template_name'] . "/data_" . $langsDatas[0].".sql");
				$demoData = str_replace("UPDATE `", "UPDATE `" . @$_POST['db_table_prefix'] . "", $demoData);
				$db->exec($demoData);
			}		
		}
	}
	catch (PDOException $e) 
	{
		echo '{"status":"DB Error:' . $e->getMessage() . '"}';
		die();
	}	
	
		// Install file structure of the choosen template
	$materials_status = '';
	try 
	{
		if (!empty($_POST['template_name']) && $_POST['template_name'] != 'default') 
		{
			$catalogs = 'data/'.$_POST['template_name'].'/materials/catalogs.zip';
			// Unzip materials
			if (file_exists($catalogs)) 
			{
				$materials = "MATADD";
				$zip = new ZipArchive;
				$res = $zip->open($catalogs);
				if ($res) 
				{
					$zip->extractTo('../materials/catalogs/');
					$zip->close();
				}
				else
					$materials = "MATERROR";
			} 
			else
				$materials = "MATERROR";

			$materials_status = ', "materials":"' . $materials . '"';
		} 
		else
			$materials_status = ', "materials":"empty"';
	}
	catch (Exception $e) 
	{
		echo '{"status":"Materials Error:' . $e->getMessage() . '"}';
		die();
	}
	
		// Create a new config file
    $fname = "../config/config.php";
	try
	{
		$f = fopen($fname, "w");
		if (!$f)
			throw new Exception('File open failed.');

		fwrite($f, $emptyConfig);
		fclose($f);
	}
	catch (Exception $e)
	{
		echo '{"status":"' . $e->getMessage(). '"}';
		die();
	}	

    echo '{"status":"done"'.$materials_status.'}';
}