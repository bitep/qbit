SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for arhlog
-- ----------------------------
DROP TABLE IF EXISTS `arhlog`;
CREATE TABLE `arhlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `hash_group` varchar(255) NOT NULL DEFAULT '',
  `reg_date` datetime NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `resource_name` varchar(255) NOT NULL,
  `priority` smallint(6) NOT NULL,
  `type` varchar(1) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `conditions` text,
  `data` mediumtext,
  `actuality` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_hash_group` (`hash_group`),
  KEY `idx_resource_name` (`resource_name`),
  KEY `idx_session_id` (`session_id`)
) DEFAULT CHARSET=utf8;



-- ----------------------------
-- Table structure for catalogs
-- ----------------------------
DROP TABLE IF EXISTS `catalogs`;
CREATE TABLE `catalogs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `design_id` int(10) NOT NULL,
  `form_id` int(10) NOT NULL,
  `catalog_name` varchar(255) NOT NULL,
  `catalog_title` varchar(255) DEFAULT NULL,
  `catalog_content` longtext,
  `catalog_group` varchar(255) NOT NULL,
  `is_category` tinyint(1) NOT NULL DEFAULT '0',
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `is_searchable` tinyint(1) NOT NULL DEFAULT '1',
  `insert_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `content_name` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '0',
  `is_searchable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for designs
-- ----------------------------
DROP TABLE IF EXISTS `designs`;
CREATE TABLE `designs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `design_name` varchar(255) NOT NULL,
  `block` text NOT NULL,
  `structure` text NOT NULL,
  `additional_style` text NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of designs
-- ----------------------------

INSERT INTO `designs`(id, design_name, block, structure, additional_style) VALUES ('0', 'admin_langbar', '<li class=\"dropdown\">\r\n<a class=\"current_lang_url\" rel=\"[[$portal_current_lang]]\" data-toggle=\"dropdown\" href=\"#\"><img src=\"[[$ADMIN_TEMPLATE_URL]]images/flags/[[$portal_current_lang]].png\"> [[$portal_current_lang]]</a>\r\n<ul class=\"dropdown-menu\" role=\"menu\">\r\n[[structure]]\r\n</ul>\r\n</li>', '<li role=\"menuitem\"><a href=\"[[language_url]]\"><img src=\"[[$ADMIN_TEMPLATE_URL]]images/flags/[[short]].png\" alt=\"[[language]]\"> [[short]]</a></li>', '');

-- ----------------------------
-- Table structure for forms
-- ----------------------------
DROP TABLE IF EXISTS `forms`;
CREATE TABLE `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `design_id` int(10) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of forms
-- ----------------------------

-- ----------------------------
-- Table structure for form_fields
-- ----------------------------
DROP TABLE IF EXISTS `form_fields`;
CREATE TABLE `form_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `field_class` varchar(255) DEFAULT NULL,
  `field_title` varchar(255) NOT NULL,
  `field_width` smallint(6) NOT NULL DEFAULT '50',
  `field_type_id` smallint(2) NOT NULL,
  `field_select_id` int(11) DEFAULT NULL,
  `linked_field_id` int(11) NOT NULL,
  `translation` smallint(1) NOT NULL DEFAULT '0',
  `required` smallint(1) NOT NULL DEFAULT '0',
  `datetime` smallint(1) NOT NULL DEFAULT '0',
  `has_placeholder` smallint(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of form_fields
-- ----------------------------

-- ----------------------------
-- Table structure for form_field_selects
-- ----------------------------
DROP TABLE IF EXISTS `form_field_selects`;
CREATE TABLE `form_field_selects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `select_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of form_field_selects
-- ----------------------------

-- ----------------------------
-- Table structure for form_field_select_options
-- ----------------------------
DROP TABLE IF EXISTS `form_field_select_options`;
CREATE TABLE `form_field_select_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_select_id` int(11) NOT NULL,
  `option_title` varchar(255) NOT NULL,
  `option_value` varchar(255) NOT NULL,
  `selected` tinyint(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `form_field_selects_fk` (`field_select_id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of form_field_select_options
-- ----------------------------

-- ----------------------------
-- Table structure for form_field_types
-- ----------------------------
DROP TABLE IF EXISTS `form_field_types`;
CREATE TABLE `form_field_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of form_field_types
-- ----------------------------
INSERT INTO `form_field_types` VALUES ('1', 'text');
INSERT INTO `form_field_types` VALUES ('2', 'textarea');
INSERT INTO `form_field_types` VALUES ('3', 'select');
INSERT INTO `form_field_types` VALUES ('4', 'button');
INSERT INTO `form_field_types` VALUES ('5', 'upload');
INSERT INTO `form_field_types` VALUES ('6', 'checkbox');
INSERT INTO `form_field_types` VALUES ('7', 'multiselect');

-- ----------------------------
-- Table structure for form_field_values
-- ----------------------------
DROP TABLE IF EXISTS `form_field_values`;
CREATE TABLE `form_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `row_id` int(11) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of form_field_values
-- ----------------------------

-- ----------------------------
-- Table structure for grants
-- ----------------------------
DROP TABLE IF EXISTS `grants`;
CREATE TABLE `grants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_name` varchar(50) NOT NULL,
  `action_name` varchar(50) DEFAULT NULL,
  `resource_id` int(11) NOT NULL DEFAULT '0',
  `grant_type` varchar(20) NOT NULL COMMENT 'CRUD',
  `resource_type` varchar(50) NOT NULL DEFAULT 'modules',
  PRIMARY KEY (`id`),
  UNIQUE KEY `full_idx1` (`resource_name`,`action_name`,`grant_type`,`resource_type`) USING BTREE,
  KEY `full_idx2` (`resource_id`,`grant_type`,`resource_type`) USING BTREE
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grants
-- ----------------------------
INSERT INTO `grants` VALUES ('1', 'layouts', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('2', 'layouts', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('3', 'layouts', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('4', 'pages', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('5', 'pages', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('6', 'pages', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('7', 'contents', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('8', 'contents', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('9', 'contents', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('10', 'subscribers', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('11', 'subscribers', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('12', 'subscribers', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('13', 'subscribers', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('16', 'catalogs', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('17', 'catalogs', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('18', 'catalogs', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('19', 'designs', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('20', 'designs', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('21', 'designs', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('22', 'forms', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('23', 'forms', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('24', 'forms', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('25', 'profiles', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('26', 'profiles', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('27', 'profiles', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('28', 'materials', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('29', 'materials', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('30', 'materials', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('32', 'admin', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('33', 'designs', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('34', 'layouts', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('35', 'pages', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('36', 'crud', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('37', 'crud', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('38', 'crud', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('39', 'crud', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('40', 'auth', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('41', 'auth', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('42', 'cms', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('43', 'cms', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('44', 'cms', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('45', 'cms', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('46', 'grants', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('47', 'grants', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('48', 'grants', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('49', 'grants', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('50', 'admin', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('51', 'admin', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('52', 'admin', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('53', 'auth', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('54', 'auth', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('56', 'catalogs', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('57', 'contents', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('58', 'forms', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('59', 'materials', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('60', 'profiles', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('61', 'search', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('62', 'search', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('63', 'search', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('64', 'search', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('65', 'updates', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('66', 'licenses', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('67', 'licenses', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('68', 'licenses', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('69', 'licenses', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('70', 'updates', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('71', 'updates', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('72', 'updates', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('73', 'translations', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('74', 'translations', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('75', 'translations', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('76', 'translations', null, '0', 'DELETE', 'modules');
INSERT INTO `grants` VALUES ('77', 'install', null, '0', 'GET', 'modules');
INSERT INTO `grants` VALUES ('78', 'install', null, '0', 'PUT', 'modules');
INSERT INTO `grants` VALUES ('79', 'install', null, '0', 'POST', 'modules');
INSERT INTO `grants` VALUES ('80', 'install', null, '0', 'DELETE', 'modules');



-- ----------------------------
-- Table structure for grant_resource_types
-- ----------------------------
DROP TABLE IF EXISTS `grant_resource_types`;
CREATE TABLE `grant_resource_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_type` varchar(100) NOT NULL,
  `has_children` smallint(1) NOT NULL DEFAULT '0',
  `field_name` varchar(100) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `get` smallint(1) NOT NULL DEFAULT '0' COMMENT 'Default access values',
  `post` smallint(1) NOT NULL DEFAULT '0',
  `put` smallint(1) NOT NULL DEFAULT '0',
  `delete` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grant_resource_types
-- ----------------------------
INSERT INTO `grant_resource_types` VALUES ('1', 'pages', '1', null, 'Pages', '0', '0', '0', '0');
INSERT INTO `grant_resource_types` VALUES ('2', 'catalogs', '1', null, 'Catalogs', '0', '0', '0', '0');
INSERT INTO `grant_resource_types` VALUES ('3', 'contents', '0', null, 'Contents', '0', '0', '0', '0');
-- ----------------------------
-- Table structure for grants_packs
-- ----------------------------
DROP TABLE IF EXISTS `grants_packs`;
CREATE TABLE `grants_packs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;
-- ----------------------------
-- Table structure for grants_packs_ref
-- ----------------------------
DROP TABLE IF EXISTS `grants_packs_ref`;
CREATE TABLE `grants_packs_ref` (
  `pack_id` int(11) NOT NULL,
  `grant_id` int(11) NOT NULL,
  KEY `full_idx1` (`pack_id`,`grant_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `short` varchar(10) NOT NULL,
  `language` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for layouts
-- ----------------------------
DROP TABLE IF EXISTS `layouts`;
CREATE TABLE `layouts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `layout_name` varchar(255) NOT NULL,
  `layout_content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for maps
-- ----------------------------
DROP TABLE IF EXISTS `maps`;
CREATE TABLE `maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lq` text NOT NULL,
  `container_type` varchar(255) NOT NULL,
  `container_id` int(11) NOT NULL,
  `resource_type` varchar(255) NOT NULL,
  `resource_name` varchar(255) NOT NULL,
  `resource_id` int(11) NOT NULL DEFAULT '0',
  `action` varchar(255) NOT NULL,
  `design` varchar(255) NOT NULL,
  `material_design` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for materials
-- ----------------------------
DROP TABLE IF EXISTS `materials`;
CREATE TABLE `materials` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `object_id` varchar(255) NOT NULL,
  `object_type` varchar(255) NOT NULL,
  `material_type` enum('image','word','pdf','excel','html','mp3','text','zip','file') NOT NULL,
  `material_title` varchar(255) NOT NULL,
  `material_path` varchar(255) NOT NULL,
  `material_insert_date` datetime NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `layout_id` int(10) NOT NULL,
  `design_id` int(10) NOT NULL DEFAULT '0',
  `page_name` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_meta_title` text NOT NULL,
  `page_meta_keywords` text NOT NULL,
  `page_meta_description` text NOT NULL,
  `page_content` longtext NOT NULL,
  `page_menu_group` varchar(255) NOT NULL,
  `page_sub_menu` varchar(255) NOT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_external_link` tinyint(1) NOT NULL DEFAULT '0',
  `external_url_target` varchar(255) NOT NULL DEFAULT '_self' COMMENT 'could be _blank or other frame name',
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for qbit_versions
-- ----------------------------
DROP TABLE IF EXISTS `qbit_versions`;
CREATE TABLE `qbit_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(255) DEFAULT NULL,
  `type` varchar(2) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qbit_versions
-- ----------------------------
INSERT INTO `qbit_versions` VALUES ('1', '3.3.2', 'f', '1439968');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Public');
INSERT INTO `roles` VALUES ('2', 'Administrator');
INSERT INTO `roles` VALUES ('3', 'Users');

-- ----------------------------
-- Table structure for role_grants
-- ----------------------------
DROP TABLE IF EXISTS `role_grants`;
CREATE TABLE `role_grants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `grant_id` int(11) DEFAULT NULL,
  `pack_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_grant_fc1` (`role_id`) USING BTREE,
  KEY `role_grant_fc2` (`grant_id`) USING BTREE,
  KEY `full_idx1` (`role_id`,`pack_id`) USING BTREE,
  KEY `full_idx2` (`role_id`,`grant_id`) USING BTREE  
) AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_grants
-- ----------------------------
INSERT INTO `role_grants` VALUES ('1', '1', '1', null);
INSERT INTO `role_grants` VALUES ('4', '1', '4', null);
INSERT INTO `role_grants` VALUES ('5', '2', '5', null);
INSERT INTO `role_grants` VALUES ('6', '2', '6', null);
INSERT INTO `role_grants` VALUES ('7', '1', '7', null);
INSERT INTO `role_grants` VALUES ('17', '1', '16', null);
INSERT INTO `role_grants` VALUES ('20', '1', '19', null);
INSERT INTO `role_grants` VALUES ('23', '1', '22', null);
INSERT INTO `role_grants` VALUES ('26', '1', '25', null);
INSERT INTO `role_grants` VALUES ('31', '1', '28', null);
INSERT INTO `role_grants` VALUES ('35', '2', '32', null);
INSERT INTO `role_grants` VALUES ('39', '1', '36', null);
INSERT INTO `role_grants` VALUES ('43', '1', '40', null);
INSERT INTO `role_grants` VALUES ('44', '1', '41', null);
INSERT INTO `role_grants` VALUES ('45', '2', '42', null);
INSERT INTO `role_grants` VALUES ('46', '2', '43', null);
INSERT INTO `role_grants` VALUES ('47', '2', '44', null);
INSERT INTO `role_grants` VALUES ('48', '2', '45', null);
INSERT INTO `role_grants` VALUES ('49', '2', '46', null);
INSERT INTO `role_grants` VALUES ('50', '2', '47', null);
INSERT INTO `role_grants` VALUES ('51', '2', '48', null);
INSERT INTO `role_grants` VALUES ('52', '2', '49', null);
INSERT INTO `role_grants` VALUES ('53', '2', '50', null);
INSERT INTO `role_grants` VALUES ('54', '2', '51', null);
INSERT INTO `role_grants` VALUES ('55', '2', '52', null);
INSERT INTO `role_grants` VALUES ('58', '2', '1', null);
INSERT INTO `role_grants` VALUES ('59', '2', '4', null);
INSERT INTO `role_grants` VALUES ('60', '2', '7', null);
INSERT INTO `role_grants` VALUES ('61', '2', '16', null);
INSERT INTO `role_grants` VALUES ('62', '2', '33', null);
INSERT INTO `role_grants` VALUES ('63', '2', '34', null);
INSERT INTO `role_grants` VALUES ('64', '2', '35', null);
INSERT INTO `role_grants` VALUES ('65', '2', '37', null);
INSERT INTO `role_grants` VALUES ('66', '2', '41', null);
INSERT INTO `role_grants` VALUES ('67', '2', '56', null);
INSERT INTO `role_grants` VALUES ('68', '2', '57', null);
INSERT INTO `role_grants` VALUES ('69', '2', '58', null);
INSERT INTO `role_grants` VALUES ('70', '2', '59', null);
INSERT INTO `role_grants` VALUES ('71', '2', '60', null);
INSERT INTO `role_grants` VALUES ('72', '2', '2', null);
INSERT INTO `role_grants` VALUES ('73', '2', '3', null);
INSERT INTO `role_grants` VALUES ('74', '2', '8', null);
INSERT INTO `role_grants` VALUES ('75', '2', '9', null);
INSERT INTO `role_grants` VALUES ('76', '2', '17', null);
INSERT INTO `role_grants` VALUES ('77', '2', '18', null);
INSERT INTO `role_grants` VALUES ('78', '2', '19', null);
INSERT INTO `role_grants` VALUES ('79', '2', '20', null);
INSERT INTO `role_grants` VALUES ('80', '2', '21', null);
INSERT INTO `role_grants` VALUES ('81', '2', '22', null);
INSERT INTO `role_grants` VALUES ('82', '2', '23', null);
INSERT INTO `role_grants` VALUES ('83', '2', '24', null);
INSERT INTO `role_grants` VALUES ('84', '2', '25', null);
INSERT INTO `role_grants` VALUES ('85', '2', '26', null);
INSERT INTO `role_grants` VALUES ('86', '2', '27', null);
INSERT INTO `role_grants` VALUES ('87', '2', '28', null);
INSERT INTO `role_grants` VALUES ('88', '2', '29', null);
INSERT INTO `role_grants` VALUES ('89', '2', '30', null);
INSERT INTO `role_grants` VALUES ('90', '2', '36', null);
INSERT INTO `role_grants` VALUES ('91', '2', '38', null);
INSERT INTO `role_grants` VALUES ('92', '2', '39', null);
INSERT INTO `role_grants` VALUES ('93', '2', '40', null);
INSERT INTO `role_grants` VALUES ('94', '2', '53', null);
INSERT INTO `role_grants` VALUES ('95', '2', '54', null);
INSERT INTO `role_grants` VALUES ('96', '1', '37', null);
INSERT INTO `role_grants` VALUES ('97', '1', '38', null);
INSERT INTO `role_grants` VALUES ('98', '1', '39', null);
INSERT INTO `role_grants` VALUES ('99', '2', '64', null);
INSERT INTO `role_grants` VALUES ('100', '2', '63', null);
INSERT INTO `role_grants` VALUES ('101', '2', '62', null);
INSERT INTO `role_grants` VALUES ('102', '2', '61', null);
INSERT INTO `role_grants` VALUES ('103', '1', '61', null);
INSERT INTO `role_grants` VALUES ('104', '2', '65', null);
INSERT INTO `role_grants` VALUES ('105', '2', '66', null);
INSERT INTO `role_grants` VALUES ('106', '1', '66', null);
INSERT INTO `role_grants` VALUES ('107', '2', '73', null);
INSERT INTO `role_grants` VALUES ('108', '2', '74', null);
INSERT INTO `role_grants` VALUES ('109', '2', '75', null);
INSERT INTO `role_grants` VALUES ('110', '2', '76', null);
INSERT INTO `role_grants` VALUES ('111', '1', '77', null);
INSERT INTO `role_grants` VALUES ('112', '1', '78', null);
INSERT INTO `role_grants` VALUES ('113', '1', '79', null);
INSERT INTO `role_grants` VALUES ('114', '1', '80', null);
INSERT INTO `role_grants` VALUES ('115', '3', '36', null);
INSERT INTO `role_grants` VALUES ('116', '3', '37', null);
INSERT INTO `role_grants` VALUES ('117', '3', '38', null);
INSERT INTO `role_grants` VALUES ('118', '3', '39', null);
INSERT INTO `role_grants` VALUES ('119', '3', '1', null);
INSERT INTO `role_grants` VALUES ('120', '3', '4', null);
INSERT INTO `role_grants` VALUES ('121', '3', '7', null);
INSERT INTO `role_grants` VALUES ('122', '3', '10', null);
INSERT INTO `role_grants` VALUES ('123', '3', '16', null);
INSERT INTO `role_grants` VALUES ('124', '3', '19', null);
INSERT INTO `role_grants` VALUES ('125', '3', '22', null);
INSERT INTO `role_grants` VALUES ('126', '3', '25', null);
INSERT INTO `role_grants` VALUES ('127', '3', '28', null);
INSERT INTO `role_grants` VALUES ('128', '3', '40', null);
INSERT INTO `role_grants` VALUES ('129', '3', '46', null);
INSERT INTO `role_grants` VALUES ('130', '3', '61', null);
INSERT INTO `role_grants` VALUES ('131', '3', '65', null);
INSERT INTO `role_grants` VALUES ('132', '3', '66', null);
INSERT INTO `role_grants` VALUES ('133', '3', '73', null);
INSERT INTO `role_grants` VALUES ('134', '3', '77', null);


-- ----------------------------
-- Table structure for subscribers
-- ----------------------------
DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for translations
-- ----------------------------
DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) NOT NULL DEFAULT '',
  `row_id` int(10) NOT NULL DEFAULT '0',
  `field_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for translation_modules
-- ----------------------------
DROP TABLE IF EXISTS `translation_modules`;
CREATE TABLE `translation_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `field` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of translation_modules
-- ----------------------------
INSERT INTO `translation_modules` VALUES ('1', 'contents', 'contents', 'content');
INSERT INTO `translation_modules` VALUES ('2', 'pages', 'pages', 'page_title');
INSERT INTO `translation_modules` VALUES ('3', 'pages', 'pages', 'page_content');
INSERT INTO `translation_modules` VALUES ('4', 'blogs', 'blogs', 'blog_title');
INSERT INTO `translation_modules` VALUES ('11', 'catalogs', 'catalogs', 'catalog_title');
INSERT INTO `translation_modules` VALUES ('12', 'catalogs', 'catalogs', 'catalog_content');
INSERT INTO `translation_modules` VALUES ('13', 'forms', 'form_fields', 'field_title');
INSERT INTO `translation_modules` VALUES ('14', 'forms', 'form_field_values', 'value');
INSERT INTO `translation_modules` VALUES ('15', 'catalog_images', 'catalog_images', 'catalog_image_title');
INSERT INTO `translation_modules` VALUES ('17', 'materials', 'materials', 'material_title');
INSERT INTO `translation_modules` VALUES ('18', 'forms', 'form_field_select_options', 'option_title');
INSERT INTO `translation_modules` VALUES ('19', 'translations', 'translations_words', 'w_value');
INSERT INTO `translation_modules` VALUES ('20', 'forms', 'form_field_select_options', 'option_title');

-- ----------------------------
-- Table structure for update_files
-- ----------------------------
DROP TABLE IF EXISTS `update_files`;
CREATE TABLE `update_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of update_files
-- ----------------------------

-- ----------------------------
-- Table structure for update_licenses
-- ----------------------------
DROP TABLE IF EXISTS `update_licenses`;
CREATE TABLE `update_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `license_key` varchar(255) DEFAULT NULL,
  `activation_datetime` datetime DEFAULT NULL,
  `deactivation_datetime` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `file_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of update_licenses
-- ----------------------------

-- ----------------------------
-- Table structure for update_license_checks
-- ----------------------------
DROP TABLE IF EXISTS `update_license_checks`;
CREATE TABLE `update_license_checks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `license_key` varchar(255) DEFAULT NULL,
  `check_datetime` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of update_license_checks
-- ----------------------------


-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `patronymic` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  `ldap_user` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for user_grants
-- ----------------------------
DROP TABLE IF EXISTS `user_grants`;
CREATE TABLE `user_grants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `grant_id` int(11) DEFAULT NULL,
  `pack_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `full_idx1` (`user_id`,`role_id`) USING BTREE,
  KEY `full_idx2` (`user_id`,`pack_id`) USING BTREE,
  KEY `full_idx3` (`user_id`,`grant_id`) USING BTREE
) AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_grants
-- ----------------------------
INSERT INTO `user_grants` VALUES ('1', '1', '2', null, null);
INSERT INTO `user_grants` VALUES ('2', '1', null, '1', null);
INSERT INTO `user_grants` VALUES ('3', '1', null, '2', null);
INSERT INTO `user_grants` VALUES ('4', '1', null, '61', null);
INSERT INTO `user_grants` VALUES ('5', '1', null, '62', null);

SET FOREIGN_KEY_CHECKS=1;