<?php

/**
 * @package    backstage
 *
 * @copyright  Copyright 2014 Rinat Gazikhanov, Vusal Khalilov, BITEP LLC. All rights reserved.
 * @license    GNU General Public License version 3 or later; see LICENSE.txt
 */
 
class Config
{	
    private function __construct() {  }
    private function __clone()     {  }
    private function __wakeup()    {  }
	
    public static function gi() 
    {
        if (is_null(self::$instance)) 
		{
            self::$instance = new Config;
        }
        return self::$instance;
    }
	
	final public static function getConfig()
	{
		return array (
                        // DB parameters
			"db_host" => "localhost",
			"db_name" => "qbit",
			"db_user" => "root",
			"db_pass" => "1986519",
			"db_table_prefix" => "",
			
			"db_adapter" => "PDODB",
			"db_install" => false,
			"db_install_option" => "create,insert",				// recreate, create, alter, update, insert, delete
			
                        // Portal parameters
			"portal_email" => "office@bitep.net",
			"portal_name" => "qBit",
			"portal_urls" => "http://localhost/qbit/",		// separated by comma
			"portal_langs" => "az,en,ru",
			"portal_default_lang" => "ru",
			"portal_time_correction" => 10,
			"portal_installed" => 1,
			"development_mode" => 0,
			
                        // Arhlog parameters
			"arhlog" => "",														// modules separated by comma
			"arhlog_disabled_tables" => "translations,translation_modules",		// tables excluded from being arhlogged
			
						// Pretorian parameters 
			"pretorian_check_level" => "m",	// (m - modules check level, a - actions check level)
			
                        // Image parameters
			"image_max_width" => 800,
			"image_max_height" => 400,
			"image_thumb_max_width" => 400,
			"image_thumb_max_height" => 200,
                        
                        // Core parameters
			"template_name" => "master",
			"admin_template_name" => "default",
			"default_module_name" => "pages",						// which module will be called by default when the portal is opened
			"default_action_name" => "getPage",
			"default_view_name" => "default",
			"default_data_type" => "text",							// default type of the returned data (json, xml or text) 
			"default_method" => "GET",								// default request method (PUT, POST, GET, DELETE - CRUD) 
			
			"controller_prefix" => "c",
			"model_prefix" => "m",
			"date_format" => "Y-m-d H:i:s",			
			
			"process_keep_time" => 30,
			"track_user_activity" => 1,			
                    
                        // Navigator parameters
			"count_per_page" => 20,

            // QBIT Update
            "update_server_address" => 'http://localhost/qbit/',
			
						// Authentication settings   ( values : simple | ldap )
			"authentication_type" => "simple", 
			
						// LDAP parameters 
			"ldaphost" => "127.0.0.1", 
			"ldapport"=>"389",
			"ldapmemberof"=>"",
			"ldapbase"=>"OU=DUMMY,DC=dummy,DC=com",
			"ldapfilter"=>"",
			"ldapdomain"=>"@dummy.com"			
		);
	}
}